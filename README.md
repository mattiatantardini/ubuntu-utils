# Ubuntu Utils
Repo to store some util files to set up Ubuntu distributions.

Current features:
- `.bashrc` file for rich terminal highlighting (included virtualenv and current git branch)
- install script for main and common ubuntu packages
