PACKAGE_LIST_FILE=$1

# Adding repository for python packages
sudo add-apt-repository ppa:deadsnakes/ppa

# Add postgresql repository
sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -

sudo apt-get -y update

# Installing ubuntu packages
< ${PACKAGE_LIST_FILE} xargs sudo apt-get install -y --fix-missing

# Optional: upgrade pip
# NOTE that this may solve problems in installing python dependencies with newer python versions
echo "upgrading pip"
curl -sS https://bootstrap.pypa.io/get-pip.py | python3.10
